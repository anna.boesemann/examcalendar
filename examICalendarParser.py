# script for parsing examination dates from flexstat and write to .ical file
## requires packages
# !pip install icalendar
# !pip install caldav==0.8.2

## import packages
import sys
import os
import argparse
import re
import requests
import urllib3
import json
import pandas as pd
from datetime import datetime, timedelta
from icalendar import Calendar, Event, vDatetime
import pytz
import caldav

def requestFlexStat(startDate, endDate):
    session = requests.Session()
    response = session.get('https://pruefungsverwaltung.uni-goettingen.de/statistikportal#parameters?query=260')
    
    request_data = {'data': '{"id":289,"title":"An-/Abmeldefristen einer Fakult\u00e4t (genaues Datum, Auflistung mit Studiengang)","description":"An- und Abmeldefristen f\u00fcr alle Pr\u00fcfungen einer Fakult\u00e4t. Es kann nur ein einzelner Tag (erstes Feld) oder ein Zeitraum angegeben werden.","outputcolumns":"Semester | Fakult\u00e4t | Studiengang | Modul | Teilmodul | UniVZ-Nummer | Pr\u00fcfungsdatum | Nachname | Vorname | LV-Titel | Bemerkung | Anmeldefrist von | Anmeldefrist bis | Abmeldefrist von | Abmeldefrist bis | Raum ","parameters":[{"name":"Fakult\u00e4t","type":0,"field":"FAK","identifier":"FAK_1_count1","optional":false,"order_count_identifier":"1_count1","associatedFields":[{"name":"Fakult\u00e4t","type":0,"lastValue":"5","lastDisplayValue":"Fakult\u00e4t f\u00fcr Mathematik und Informatik","selectAllDummy":false,"identifier":"FAK_1_count1","field":"fak","optional":false}]},{"name":"Exaktes Datum / Zeitraum von","type":5,"field":"DATUM","identifier":"DATUM_2_count1","optional":false,"order_count_identifier":"2_count1","associatedFields":[{"name":"Exaktes Datum / Zeitraum von","type":5,"lastValue":"' + startDate + '","lastDisplayValue":"' + startDate + '","selectAllDummy":false,"identifier":"DATUM_2_count1","field":"Datum","optional":false}]},{"name":"Zeitraum bis","type":5,"field":"DATUM","identifier":"DATUM_3_count1","optional":true,"order_count_identifier":"3_count1","associatedFields":[{"name":"Zeitraum bis","type":5,"lastValue":"' + endDate + '","lastDisplayValue":"' + endDate + '","selectAllDummy":false,"identifier":"DATUM_3_count1","field":"datum","optional":true}]},{"name":"ZESS-Kurse mit ausgeben","type":1,"field":"CHECKBOX","identifier":"CHECKBOX_4_count1","optional":true,"order_count_identifier":"4_count1","associatedFields":[{"name":"ZESS-Kurse mit ausgeben","type":1,"lastValue":"false","lastDisplayValue":"Nein","selectAllDummy":false,"identifier":"CHECKBOX_4_count1","field":"CheckBox","optional":true}]}]}'}

    response = session.post('https://pruefungsverwaltung.uni-goettingen.de/statistikportal/api/queryexecution/results', data = request_data)
    return json.loads(response.text)

def printMeta(data):
    print("Main keys of request response: ", data['data'].keys())
    n = data['data']['total'] # total number of entries/rows
    print("Total number of entries in dataset: ", n)
    timestamp = data['data']['metaData']['executionTimestamp'] # execution time of last request
    print("Execution time of last request to FlexStat:", timestamp)
    columns = [x['name'] for x in data['data']['metaData']['fields']]
    print("Column names of single examination event: ", columns)

def extractData(data):
    # extract exam data
    df_dict = []
    modul2course_dict = []
    modul2lvtitle = []
    for record in data['data']['records']:
        try:
            modulNumber = record['Teilmodul'].split(':')[0]
            if 'Inf' in modulNumber or 'Mat' in modulNumber:
                modulName = record['Teilmodul'].split(':')[1]
                examDate = datetime.strptime(record['Prüfungsdatum'].split(' ',1)[0], '%d.%m.%Y')
                examStartTime = datetime.combine(examDate
                            , datetime.strptime(record['Prüfungsdatum'].split(' ',1)[1][1:].split(' ',1)[0], '%H:%M').time())
                examEndTime = datetime.combine(examDate
                            , datetime.strptime(record['Prüfungsdatum'].split(' ',1)[1][:-1].rsplit(' ',1)[1], '%H:%M').time())
                studyCourse = record['Studiengang']
                lecturer = record['Vorname'] + " " + record['Nachname']
                room = record['Raum']
                lvTitle = record['LV-Titel'].replace('(online)', '')
                if len(lvTitle) < 2 :
                    lvTitle = modulName
                df_dict.append({'Modulnummer' : modulNumber
                        , 'Modulname' : modulName
                       , 'Datum' : examDate
                       , 'Startzeit' : examStartTime 
                       , 'Endzeit' : examEndTime 
                       , 'Lehrveranstaltung' : lvTitle
                       , 'Raum' : room
                        , 'DozentIn' : lecturer              
                }) 
                modul2course_dict.append({'Modulnummer' : modulNumber, 'Modulname': modulName, 'Studiengang': studyCourse})
                modul2lvtitle.append({'Modulnummer': modulNumber, 'Modulname': modulName, 'Lehrveranstaltung': lvTitle})
        except: 
            pass

    df = pd.DataFrame.from_dict(df_dict)
    df.drop_duplicates(inplace = True, subset = ['Lehrveranstaltung', 'Startzeit'])
    df_modul2course = pd.DataFrame.from_dict(modul2course_dict)
    df_modul2lvtitle = pd.DataFrame.from_dict(modul2lvtitle)
    df_modul2lvtitle.drop_duplicates(inplace = True)
    return df, df_modul2course, df_modul2lvtitle
    
def createCalendar(df, df_modul2lvtitle, url):
    if url:
        client = caldav.DAVClient(url)
        principal = client.principal()
        calendars = principal.calendars()
        # calendars is an array, one of them is exam_calendar
        cals = calendars[1]
        for event in cals.events():
            event.delete()

    cal = Calendar()
    cal.add('prodid', '-//Examdate overview//fginfo//')
    cal.add('version', '2.0')

    for index, row in df.iterrows():
        if url:
            cal_tmp = Calendar()
            cal_tmp.add('prodid', '-//Examdate overview//fginfo//')
            cal_tmp.add('version', '2.0')
        event = Event()    
        event.add('summary', row['Lehrveranstaltung'])
        if row['Startzeit'] == row['Endzeit']:
            event.add('dtstart', row['Startzeit'].date())
            event.add('dtend', row['Endzeit'].date())
        else: 
            event.add('dtstart', row['Startzeit'])
            event.add('dtend', row['Endzeit'])
        event.add('dtstamp', datetime.now())
        event.add('uid', "{}-{}-fginfo-exam-overview".format(datetime.now(),row['Modulnummer']))
        modulList = [rowx['Modulnummer'] + ": " + rowx['Modulname'] + "\n" 
                     for index, rowx in df_modul2lvtitle[df_modul2lvtitle['Lehrveranstaltung'] == row['Lehrveranstaltung']].iterrows()]
        modulList_str = ''.join(modulList)
        description = "Lehrveranstaltung: {}\nDozentIn: {}\nModule:\n{}".format(
            row['Lehrveranstaltung'], row['DozentIn'], modulList_str)
        event.add('description', description)
        event.add('location', row['Raum'])
        cal.add_component(event)
        if url:
            cal_tmp.add_component(event)
            cals.save_event(cal_tmp.to_ical())

    file = open('examcalendar.ics', 'wb')
    file.write(cal.to_ical())
    file.close()
    print("Successfully created .ics file.")
    return cal.to_ical()

if __name__ == "__main__":
    tz = pytz.timezone('Europe/Berlin')
    verbose = False
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", help="start date of the considered period")
    parser.add_argument("-e", help="end date of the considered period")
    args = parser.parse_args()
    
    try:
        url = os.environ['URL']
    except:
        url = None

    if verbose:
        print(args)

    if args.s:
        startDate = datetime.strptime(args.s, '%d.%m.%Y') # '01.01.2021'
    else:
        #startDate = datetime.strptime('01.01.2021','%d.%m.%Y')
        startDate = datetime.now() - timedelta(weeks=2)
    if verbose:
        print(startDate)

    if args.e:
        endDate = datetime.strptime(args.e, '%d.%m.%Y') # '31.03.2021'
    else:
        #endDate = datetime.strptime('31.03.2021','%d.%m.%Y')
        endDate = datetime.now() + timedelta(weeks=12)
    if verbose:
        print(endDate)

    d1 = startDate.strftime('%d.%m.%Y')
    d2 = endDate.strftime('%d.%m.%Y')
    print(d1, d2)
    data = requestFlexStat(d1,d2)
    if verbose:
        printMeta(data)
    df, df_modul2course, df_modul2lvtitle = extractData(data)
    cal = createCalendar(df, df_modul2lvtitle, url)
